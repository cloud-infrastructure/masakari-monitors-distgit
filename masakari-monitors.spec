%{!?upstream_version: %global upstream_version %{version}%{?milestone}}
%global service masakari-monitors

Name:           masakari-monitors
Version:        6.0.0
Release:        0.3%{?dist}
Summary:        Monitors for Virtual Machine High Availability (VMHA) service in OpenStack
License:        ASL 2.0
URL:            https://launchpad.net/masakari-monitors
Source0:        https://tarballs.openstack.org/%{service}/%{service}-%{upstream_version}.tar.gz
#

# Systemd scripts
Source1:        masakari-hostmonitor.service
Source2:        masakari-instancemonitor.service
Source3:        masakari-processmonitor.service
Source4:        masakari-monitors.tmpfiles
Source5:        masakari-introspectiveinstancemonitor.service

Patch0001:      0001-Use-the-connection-uri-from-configuration-instead-of.patch

BuildArch:      noarch

BuildRequires:  git
BuildRequires:  openstack-macros
BuildRequires:  python2-devel
BuildRequires:  python2-keystoneauth1 >= 3.10.0
BuildRequires:  python2-oslo-config >= 2:5.2.0
BuildRequires:  python2-oslo-log >= 3.36.0
BuildRequires:  python2-oslo-middleware >= 3.31.0
BuildRequires:  python2-pbr >= 2.0.0
BuildRequires:  systemd

%description
Monitors for the Masakari service for Virtual Machine
High Availability (VMHA) service in OpenStack.

%package -n     python-%{service}
Summary:        Masakari-Monitors Python libraries
Provides:       python-%{name} = %{version}-%{release}

Requires:       python2-automaton >= 1.9.0
Requires:       libvirt-python >= 3.9.0
Requires:       python2-pbr >= 2.0.0
Requires:       python2-six >= 1.10.0
Requires:       python2-openstacksdk >= 0.13.0
Requires:       python2-oslo-concurrency >= 3.26.0
Requires:       python2-oslo-config >= 2:5.2.0
Requires:       python2-oslo-cache >= 1.26.0
Requires:       python2-oslo-i18n >= 3.15.3
Requires:       python2-oslo-log >= 3.36.0
Requires:       python2-oslo-middleware >= 3.31.0
Requires:       python2-oslo-privsep >= 1.23.0
Requires:       python2-oslo-service >= 1.24.0
Requires:       python2-oslo-utils >= 3.33.0
Requires:       python-lxml

%description -n python-%{service}
Masakari is a service providing high availability for VMs in OpenStack.

This package contains the Python libraries for the monitoring components.


%package -n openstack-masakari-hostmonitor
Summary:        Host monitoring component of OpenStack Masakari service
Requires:       %{name} = %{version}
Requires:       python-%{service} = %{version}-%{release}

%description -n openstack-masakari-hostmonitor
This component of the Masakari service for VM HA in OpenStack is
responsible for monitoring the health of nova-compute hypervisors.

%package -n openstack-masakari-instancemonitor
Summary:        Instance monitoring component of OpenStack Masakari service
Requires:       %{name} = %{version}
Requires:       python-%{service} = %{version}-%{release}

%description -n openstack-masakari-instancemonitor
This component of the Masakari service for VM HA in OpenStack is
responsible for monitoring the health of individual VM instances.

%package -n openstack-masakari-introspectiveinstancemonitor
Summary:        Instance monitoring component of OpenStack Masakari service
Requires:       %{name} = %{version}
Requires:       python-%{service} = %{version}-%{release}

%description -n openstack-masakari-introspectiveinstancemonitor
This component of the Masakari service for VM HA in OpenStack is
responsible for monitoring the health of individual VM instances.

%package -n openstack-masakari-processmonitor
Summary:        Process monitoring component of OpenStack Masakari service
Requires:       %{name} = %{version}
Requires:       python-%{service} = %{version}-%{release}

%description -n openstack-masakari-processmonitor
This component of the Masakari service for VM HA in OpenStack is
responsible for monitoring the health of processes which need to stay
running in order for the compute plane to be functional and highly
available.

%prep
%autosetup -n masakari-monitors-%{upstream_version} -S git
%py_req_cleanup

%build
export PYTHONPATH="."
# from tox.ini's testenv:genconfig
oslo-config-generator --config-file etc/masakarimonitors/masakarimonitors-config-generator.conf
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}/masakarimonitors/
mkdir -p %{buildroot}%{_localstatedir}/log/masakari-monitors

# These are broken (hard-coded references to /usr/local/lib/python2.7)
# and provide nothing which can't be handled better by systemd,
# so we'll use systemd services instead.
rm %{buildroot}%{_bindir}/masakari-{host,process}monitor.sh*

#systemd unitfiles
install -p -D -m 644 %SOURCE1 %{buildroot}%{_unitdir}/openstack-masakari-hostmonitor.service
install -p -D -m 644 %SOURCE2 %{buildroot}%{_unitdir}/openstack-masakari-instancemonitor.service
install -p -D -m 644 %SOURCE3 %{buildroot}%{_unitdir}/openstack-masakari-processmonitor.service
install -p -D -m 644 %SOURCE5 %{buildroot}%{_unitdir}/openstack-masakari-introspectiveinstancemonitor.service

install -p -D -m 644 %SOURCE4 %{buildroot}/%{_tmpfilesdir}/masakari-monitors.conf

install -p -D -m 640 etc/masakarimonitors/masakarimonitors.conf.sample %{buildroot}%{_sysconfdir}/masakarimonitors/masakarimonitors.conf
install -p -D -m 640 etc/masakarimonitors/process_list.yaml.sample %{buildroot}%{_sysconfdir}/masakarimonitors/process_list.yaml
chmod +x %{buildroot}%{_bindir}/masakari-*

%pre
%openstack_pre_user_group_create masakari masakari

%post
%tmpfiles_create %{_tmpfilesdir}/masakari-monitors.conf

%post -n openstack-masakari-hostmonitor
%systemd_post openstack-masakari-hostmonitor.service

%preun -n openstack-masakari-hostmonitor
%systemd_preun openstack-masakari-hostmonitor.service

%postun -n openstack-masakari-hostmonitor
%systemd_postun_with_restart openstack-masakari-hostmonitor.service

%post -n openstack-masakari-instancemonitor
%systemd_post openstack-masakari-instancemonitor.service

%preun -n openstack-masakari-instancemonitor
%systemd_preun openstack-masakari-instancemonitor.service

%postun -n openstack-masakari-instancemonitor
%systemd_postun_with_restart openstack-masakari-instancemonitor.service

%post -n openstack-masakari-processmonitor
%systemd_post openstack-masakari-processmonitor.service

%preun -n openstack-masakari-processmonitor
%systemd_preun openstack-masakari-processmonitor.service

%postun -n openstack-masakari-processmonitor
%systemd_postun_with_restart openstack-masakari-processmonitor.service

%files
%license LICENSE
%dir %{_sysconfdir}/masakarimonitors
%config(noreplace) %attr(-, masakari, masakari) %{_sysconfdir}/masakarimonitors/masakarimonitors.conf
%dir %attr(750, masakari, masakari) %{_localstatedir}/log/masakari-monitors
%_tmpfilesdir/masakari-monitors.conf

%files -n openstack-masakari-hostmonitor
%{_bindir}/masakari-hostmonitor
%{_unitdir}/openstack-masakari-hostmonitor.service

%files -n openstack-masakari-instancemonitor
%{_bindir}/masakari-instancemonitor
%{_unitdir}/openstack-masakari-instancemonitor.service

%files -n openstack-masakari-introspectiveinstancemonitor
%{_bindir}/masakari-introspectiveinstancemonitor
%{_unitdir}/openstack-masakari-introspectiveinstancemonitor.service

%files -n openstack-masakari-processmonitor
%{_bindir}/masakari-processmonitor
%{_unitdir}/openstack-masakari-processmonitor.service
%config(noreplace) %attr(-, masakari, masakari) %{_sysconfdir}/masakarimonitors/process_list.yaml

%files -n python-%{service}
%license LICENSE
%{python2_sitelib}/masakari*

%changelog
* Wed Mar 27 2019 Jose Castro Leon <jose.castro.leon@cern.chject.org> 6.0.0-0.3
- Fix bug on introspectivemonitor to be able to use it as a normal user

* Fri Mar 22 2019 Jose Castro Leon <jose.castro.leon@cern.chject.org> 6.0.0-0.2
- Fix missing configuration and avoid shipping unneeded files

* Wed Mar 06 2019 Jose Castro Leon <jose.castro.leon@cern.chject.org> 6.0.0-0.1
- Initial version of the rpm
